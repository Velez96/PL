#include <stdio.h>
#include <stdlib.h>

	int columna=0,fila=0,cont=0,i=0,j=0;
	int arr[5][9]={ {0,2,0,2,9,0,0,0,0},
					{0,0,0,0,2,2,2,2,0},
					{0,2,2,0,0,0,0,2,0},
					{0,2,0,2,2,2,0,2,0},
					{0,2,0,0,0,0,0,0,0} };
					
int checarPosicion(int filas, int columnas);
void imprimirArreglo();
int main()
{

	imprimirArreglo();
	if(checarPosicion(fila,columna)==1){
		printf("Se ha llegado a la meta\n");
	}else{
		printf("Se murió\n");
	}
	imprimirArreglo();

	return 0;

}

int checarPosicion(int filas, int columnas){
		
	printf("el pacman esta en [%d][%d]\n",filas,columnas);

	if(filas<0||filas>4||columnas<0||columnas>8){
		return 0;
	}
	if(arr[filas][columnas]==9){
		return 1;
	}
	if(arr[filas][columnas]!=0){
		return 0;
	}
	arr[filas][columnas]=1;

	if (checarPosicion(filas,columnas-1)==1){
		return 1;
	}
	if (checarPosicion(filas+1,columnas)==1){
		return 1;
	}
	if (checarPosicion(filas,columnas+1)==1){
		return 1;
	}
	if (checarPosicion(filas-1,columnas)==1){
		return 1;
	}
	arr[filas][columnas]=8;
	
	return 0;
}
void imprimirArreglo(){
	printf("0 - Camino sin recorrer\n");
	printf("1 - Camino ya recorrido\n");
	printf("8 - Camino equivocado\n");
	printf("2 - Pared\n");
	printf("9 - Meta\n");	

	for( i = 0; i < 5; i++){
		for ( j = 0; j < 9; j++){
			printf("%d", arr[i][j]);
		}printf("\n");
	}
}

