(defun f(a)
(if (> a 100)
	(- a 10)
	(f(f(+ a 11)))
	))

(defun fibo(a)
(if (= a 0)
	0
	(if (= a 1)
		1
	(+ (fibo(- a 1)) (fibo(- a 2)))
		)
	))