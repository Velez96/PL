;-------------- 1 ------------------------------------
(defun PALINDROMOP(lista)
(if (equal (reverse lista) lista)
1; Es Palindroma
0; NOO es Palindroma
)
)

(defun PALINDROMO(lista)
(if (= (PALINDROMOP lista) 1)
(format t "Palindromo : ~a ~%" (reverse lista))
(format t "La lista NO!! es palindroma ~%")
)
)

(setq miLista '(4 2 1 2 4))
(PALINDROMO miLista)

;--------------- 2a ------------------------------------

(defun lengthR(lista cont)
(if (equal lista '())
	(format t "La longitud de la lista es: ~a ~%" cont)
	(lengthR (setq lista (remove (car lista) lista)) (+ cont 1))
))

(lengthR '( 1 2 3 4 5 6 7) 0)

;--------------- 2b ------------------------------------

(defun memberR(lista miembro)
(if (eq (find miembro lista) nil)
	(format t "~a ~%" nil)
	(format t "~a ~%" lista)
))

(memberR '( 1 2 3 4 5 6 7) 0)

;--------------- 2b ------------------------------------

(defun reverseR(lista listaAux)
(if (equal lista '())
(format t "La Lista Inversa es: ~a ~%" listaAux)
(progn
(push (car lista) listaAux)
(reverseR (setq lista (remove (car lista) lista)) listaAux)
)
)
)

(reverseR '(1 2 3) '())

;--------------- 3 ------------------------------------

