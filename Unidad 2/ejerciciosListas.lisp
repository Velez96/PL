;--------- 1 ---------------------
(setq x1 '(COCHE MOTO TREN))
(setq x2 '(EDUARDO PEDRO ANTONIO))

;--------- 2 -----------------------
(print (length (append x1 x2))) ;longitud de un lista resultante de la concatenacion de otras dos listas

;--------- 3 , 4a ------------------------
(setq nuevaLista '()) ;nuevaLista vacia
(push (car (last x2)) nuevaLista) ;de una lista que contiene el ultimo
(push (car (last x1)) nuevaLista)  ;elemento de otra lista se obtiene su primer
                                     ;valor y ese valor se inserta en nuevaLista
(print nuevaLista) ;lista (TREN ANTONIO)

;--------- 4b ---------------------------
(setq newList '())
(push (last x2) newList) ;se inserta una lista con el ultimo valor
(push (last x1) newList)  ;de otra lista a newList
(print newList) ;lista ((TREN) (ANTONIO))

;--------- 4c -----------------------------
(setq nuevaLista '())
(push (car (last x2)) nuevaLista)
(push (last x1) nuevaLista)
(print nuevaLista) ;lista ((TREN) ANTONIO)

;--------- 5 ------------------------------
(setq x3 (append x1 (reverse x2)))
(print x3)

;;; funciones ;;;;;;;;;;;;;
(defun tercerElemento(lista)
	(setq aux (car (cddr lista)))
	aux
	)

(defun creaLista(dato)
	(setq nl '())
	(push (car (last dato)) nl)
		(push (car dato) nl)
	nl
	)

(defun añadeElemento(dato xxx)
	(if (eq (find dato xxx) nil)
		(push dato xxx)

	)
	xxx
)

(defun ROTAIZQ(lista)
	(setq aux (car lista))
	(setq lista (remove (car lista) lista))
	(setq listaAux (reverse lista))
	(push aux listaAux)
	(setq lista (reverse listaAux))
	lista
)


(setq tercero (tercerElemento x3))
(print tercero)

(setq listaCreada (creaLista x3))
(print listaCreada)

(setq x3 (añadeElemento 'COCHE x3))
(print x3)

(setq listaRotaIzq (ROTAIZQ x3))
(print listaRotaIzq)