(defun cuadradosLista(lista listAux cont)
	(if (= (length lista) cont)
		(format t " ~a ~%" (reverse listAux))
	(progn
		(push (expt (nth cont lista) 2) listAux)
		(cuadradosLista lista listAux (+ cont 1))
	)
	)
)

(cuadradosLista '(2 5 3) '() 0)

(defun sust1(lista asoc listAux cont)
  (if (= (length asoc) cont)
		(format t "~a ~%" (reverse listAux))
	(progn
	(if (equal (assoc (nth cont asoc) lista) nil)
		(push (nth cont asoc) listAux)
	(progn
		(setq num (cadr (assoc (nth cont asoc) lista)))
		(if (equal (assoc num lista) nil)
					(push num listAux)
					(push (cadr (assoc num lista)) listAux)
		)
	)
	)
			(sust1 lista asoc listAux (+ cont 1))
	)
  )
)
	

(defun sust2(lista asoc listAux cont)
  (if (= (length asoc) cont)
		(format t "~a ~%" (reverse listAux))
	(progn
		(if (equal (assoc (nth cont asoc) lista) nil)
			(push (nth cont asoc) listAux)
			(push (cadr (assoc (nth cont asoc) lista)) listAux)
		)
				(sust2 lista asoc listAux (+ cont 1))
	)
  )
)
	

(sust1 '((A B) (C D) (E F) (B K) (D L)) '(A C E B D M) '() 0)

(sust2 '((A B) (C D) (E F) (B K) (D L)) '(A C E B D M) '() 0)


(defun descuentoClientes(monto)
	(cond
		((< monto 500) (format t "No hay descuento ~%"))
		((and (< monto 1000) (> monto 500)) (format t "Tienes un descuento del 5%, queda: ~a ~%" (- monto (* monto 0.05))))
		((and (< monto 7000) (> monto 1000)) (format t "Tienes un descuento del 11%, queda: ~a ~%" (- monto (* monto 0.11))))
		((and (< monto 15000) (> monto 7000)) (format t "Tienes un descuento del 18%, queda: ~a ~%" (- monto (* monto 0.18))))
		((> monto 1500) (format t "Tienes un descuento del 25%, queda: ~a ~%" (- monto (* monto 0.25))))
	)
)

(descuentoClientes 8000)


(defun sueldoTrabajador(sueldo)
		(if (< sueldo 1000)
			(format t "Tienes un aumento del 15%, queda: ~a ~%" (+ sueldo (* sueldo 0.15)))
			(format t "Tienes un aumento del 12%, queda: ~a ~%" (+ sueldo (* sueldo 0.12)))
		)
)

(sueldoTrabajador 1500)

(defun Alumno()
  (let ((matricula)(semestre)(promedio))
	(format t "Ingrese matricula: ~%")
	(setq matricula (read))
	(format t "Ingrese semestre: ~%")
	(setq semestre (read))
	(format t "Ingrese promedio: ~%")
	(setq promedio (read))
		(loop
		(unless (not (and (> semestre 6) (>= promedio 8.8))) (format t "Aceptado: ~A : INGENIERIA" matricula) (return t))
			(unless (not (and (>= semestre 6) (> promedio 8.5))) (format t "Aceptado: ~A : MEDICINA" matricula) (return t))
				(unless (not (and (> semestre 5) (>= promedio 8.3))) (format t "Aceptado: ~A : LICENCIATURA" matricula) (return t))
					(unless (not (and (>= semestre 3) (>= promedio 7.8))) (format t "Aceptado: ~A : TECNICO" matricula) (return t))
						(format t "RECHAZADO! :(") (return t)
		)
  ) 

)

(Alumno)