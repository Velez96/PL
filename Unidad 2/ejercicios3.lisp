;;;;;;;;;;;;;;;;;;;;;;;;;	1	;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defun listasRecursivas(lista longitud)
	(if (equal lista nil)
		(format t "Tus listas tienen el mismo numero de elementos ~%")
	(if(= (length (car lista)) longitud)
			(listasRecursivas (cdr lista) longitud)
			(format t "Una lista tiene un numero de elementos diferente ~%")
	)
	)
	

)

(defun menulistasRecursivas()
(setq listota '())
(loop
	(format t "Desea agregar otra lista a la funcion? S/N ~%")
	(setq op (read))
(when (or (string= op "N") (string= op "n")) (return t))
(format t "Ingrese nueva lista: ej (1 2 3) ~%")
(setq lista (read))
(push lista listota)
)
	(listasRecursivas listota (length (car listota)))

)

(menulistasRecursivas)

;;;;;;;;;;;;;;;;;;;;;;;;;	2	;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun AGRUPAR(lista elemento)
	(let ((listaAux))
 (setq listaAux '())
	(loop
		(when (equal lista nil) (return t))
		(when (eq elemento (car lista))  (push elemento lista) (return t))
		(push (car lista) listaAux)
		(setq lista (cdr lista))
	)
	(format t "-> ~a ~%" (append (reverse listaAux) lista))
))

(AGRUPAR '(A A A B B B C C C) 'B)

;;;;;;;;;;;;;;;;;;;;;;;;;	3	;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun FECHAP(dia mes año)
(if (or (or (< dia 1) (> dia 31) (< mes 0) (> mes 12) (> año 9999)) (and (= mes 2) (> dia 29)))
(print nil)
(print t)
)
)

(FECHAP 12 12 1986)
(FECHAP 12 30 1986)
(FECHAP 31 2 1986)
(FECHAP 31 11 1986)

;;;;;;;;;;;;;;;;;;;;;;;;;	4	;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun numElementosAntes(lista element cont)
(if (equal lista nil)
(format t "~% Adios")

(if (eq (car lista) element)
(format t "~% Numero de elementos antes: ~a" cont)
(numElementosAntes (cdr lista) element (+ cont 1))
)
)

)

(numElementosAntes '(9 8 3 6 5 4) '3 0)